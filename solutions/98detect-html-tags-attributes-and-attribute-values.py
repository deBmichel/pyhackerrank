#Url: https://www.hackerrank.com/challenges/detect-html-tags-attributes-and-attribute-values/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

from html.parser import HTMLParser

# In documentation tecnically the class is not needed
# But I can not understand why with only HTMLParser
# FAILS ---------YOU ARE GOING TO KILL DEAR PYTHON 3
class THeHTMLParser(HTMLParser):
    def handle_starttag(self, thetag, attributes):
        print(thetag)
        # The format here is tricky 
        [print('-> {} > {}'.format(*atr)) for atr in attributes]

if __name__ == "__main__":
	lines = int(input())
	#Again the end of line, it is dirty for me 
	#but really functional ....... :)
	html = '\n'.join([input() for _ in range(lines)])
	parser = THeHTMLParser()
	parser.feed(html)
	parser.close()
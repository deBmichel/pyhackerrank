#Url: https://www.hackerrank.com/challenges/np-arrays/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

def arrays(arr):
    # complete this function
    # use numpy.array
    numpy.set_printoptions(legacy='1.13')
    return numpy.flipud(numpy.array(arr,float))

arr = input().strip().split(' ')
result = arrays(arr)
print(result)
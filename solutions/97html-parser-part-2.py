#Url: https://www.hackerrank.com/challenges/html-parser-part-2/problem?h 
#Python Python 3.8.2 
#@Author: Michel M S 

from html.parser import HTMLParser
class CustomHTMLParser(HTMLParser):
	#Is needed overwrite ....
    def handle_comment(self, data):
        number_of_line = len(data.split('\n'))
        if number_of_line>1:
        	#It is not cool
            print('>>> Multi-line Comment')
        else:
        	#It is not cool
            print('>>> Single-line Comment')
        if data.strip():
        	#It is not cool
            print(data)

    def handle_data(self, data):
        if data.strip():
            print(">>> Data")
            print(data)

parser = CustomHTMLParser()

if __name__ == "__main__":
	lines = int(input())
	html_string = ''
	for i in range(lines):
		# The end of lines are a serious serious problems here ....
	    html_string += input().rstrip()+'\n'
	    
	parser.feed(html_string)
	parser.close()
#Url: https://www.hackerrank.com/challenges/py-introduction-to-sets/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def average(array):
    return f'{float(sum(set(array))/ len(set(array))):.3f}'

if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().split()))
    result = average(arr)
    print(result)
#Url: https://www.hackerrank.com/challenges/py-collections-namedtuple/problem?h_r=next-chal 
#Python Python 3.8.2 
#@Author: Michel M S 

# I wrotte the lines by myself and the plan was search a namedtuple method 
# To init named tuple from list, the method exists and the name is _make.
from collections import namedtuple
rows, Row, total =  int(input()), namedtuple('Row',input().split()), 0
print (f"{float(sum(  [int(x.MARKS) for x in [Row._make(input().split()) for _ in range(rows)]]  ) / rows):.2f}")


#[for i in range(input())]
"""
if __name__ == "__main__":
	test, itest = int(input()), 0
	rowids = input().split()
	Localizer = namedtuple('Localizer','position, id')
	MARKS = Localizer("Marks", "0")

	i, iflg = 0, len(rowids)
	while i < iflg:
		if rowids[i] == "MARKS":
			loc = Localizer(i, "MARKS")
			break
		i += 1

	marks_acum, marks_counter = 0,0
	while itest < test:
		marks_acum += int(input().split()[loc.position])
		marks_counter += 1
		itest += 1

	print (f"{float(marks_acum / marks_counter):.2f}")
"""
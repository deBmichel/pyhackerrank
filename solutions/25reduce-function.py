#Url: https://www.hackerrank.com/challenges/reduce-function/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from math import gcd
from collections import namedtuple
from fractions import Fraction
from functools import reduce

lcm = lambda x,y : x * y // gcd(x, y)
product = lambda x, y : x * y

def product(fracs):
	t = Fraction(reduce(lambda x, y: x*y, [e.numerator for e in fracs]),reduce(lambda x, y: x*y, [e.denominator for e in fracs]))
	return t.numerator, t.denominator

if __name__ == '__main__':
    fracs = []
    for _ in range(int(input())):
        fracs.append(Fraction(*map(int, input().split())))
    result = product(fracs)
    print(*result)

"""if __name__ == "__main__":
	numbers = int(input())
	ln, ld = [], list()
	for _ in range(numbers):
		n,d = list(map(int, input().split()))
		ln.append(n)
		ld.append(d)
	pn, pd = reduce(product, ln), reduce(product, ld)
	print(f'{lcm(pn, pd)// pd} {lcm(pn, pd)// pn}')


namedtuple('Row','numerator,denominator')._make([2,4])"""
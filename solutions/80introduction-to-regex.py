#Url: https://www.hackerrank.com/challenges/introduction-to-regex/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
import re

def validate(number):
	return bool(re.match(r'^[-+]?[0-9]*\.[0-9]+$', number))

#https://www.youtube.com/watch?v=K8L6KVGG-7o

if __name__ == "__main__":
	cases = int(input())
	for _ in range(cases):
		print(validate(input()))



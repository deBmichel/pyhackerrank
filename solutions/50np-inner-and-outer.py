#Url: https://www.hackerrank.com/challenges/np-inner-and-outer/problem 
#Python Python 3.8.2 
#@Author: Michel M S 


import numpy

if __name__ == "__main__":
	the_array_a = numpy.array(list(map(int, input().split())))
	the_array_b = numpy.array(list(map(int, input().split())))
	print (numpy.inner(the_array_a, the_array_b))
	print (numpy.outer(the_array_a, the_array_b))

#Url: https://www.hackerrank.com/challenges/input/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

# Made in python 2 , beautifull exercise about command prompt

import re

if __name__ == "__main__":
	x, y = list(map(str, raw_input().split()))
	p = re.compile('(x)')
	preparedline = p.subn(x, raw_input())
	d = str(eval(preparedline[0]))
	if d == y:
		print True
	else:
		print False


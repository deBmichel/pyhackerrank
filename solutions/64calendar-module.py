#Url: https://www.hackerrank.com/challenges/calendar-module/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
import calendar
import datetime

if __name__ == "__main__":
	month, day, year = list(map(int,input().split()))
	print(calendar.day_name[datetime.datetime(year,month,day).weekday()].upper() )

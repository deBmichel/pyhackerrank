#Url: https://www.hackerrank.com/challenges/np-transpose-and-flatten/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
import numpy

if __name__ == "__main__":

	the_n_size, the_m_size = list(map(int, input().split()))
	numpy.set_printoptions(legacy='1.13')
	rows = []
	for _ in range(the_n_size):
		rows.append(list(map(int, input().split())))
	the_array = numpy.array(rows)
	print (numpy.transpose(the_array))
	print (the_array.flatten())

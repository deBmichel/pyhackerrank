#Url: https://www.hackerrank.com/challenges/python-time-delta/problem 
#Python Python 3.8.2 
#@Author: Michel M S 



# Complete the time_delta function below.
def time_delta(t1, t2):
	from dateutil import parser
	
	datea = parser.parse(t1)
	dateb = parser.parse(t2)
	
	if datea > dateb:
	    td = datea - dateb
	else:
	    td = dateb - datea
	td_mins = int(td.total_seconds())
	return str(td_mins)

	


if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        t1 = input()
        t2 = input()
        print(time_delta(t1, t2))

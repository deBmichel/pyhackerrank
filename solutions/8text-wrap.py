#Url: https://www.hackerrank.com/challenges/text-wrap/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import textwrap

# I must be very attentive to what hackerrank has in imports and tricks

"""
def wrap(string, max_width):
	sublines, strres, i, i2, i3 = 0, '''\
''', 0, 0, max_width
	if len(string) % max_width == 0:
		sublines = len(string) // max_width
	else: 
		sublines = (len(string) // max_width) + 1
	while i <= sublines:
		strres += string[i2:i3] + '''
'''
		i2, i3= i3, max_width * (i + 1)
		i += 1
	return strres
"""

def wrap(string, max_width):
	return textwrap.fill(string, width=max_width)
	
if __name__ == '__main__':
	string, max_width = input(), int(input())
	result = wrap(string, max_width)
	print(result)
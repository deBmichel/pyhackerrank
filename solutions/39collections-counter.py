#Url: https://www.hackerrank.com/challenges/collections-counter/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from collections import Counter

if __name__ == "__main__":
	input()
	sizes = list(map(int, input().split()))
	count = Counter(sizes)
	tem   = dict.fromkeys(count.keys())
	clients = int(input())
	i, money = 0, 0
	while i < clients:
		size, val = list(map(int, input().split()))
		if size in tem:
			if tem[size] is None:
				tem[size] = 1
				money += val
			else:
				if tem[size] < (count[size]):
					tem[size] = tem[size] + 1
					money += val	
		i += 1
	print(money)


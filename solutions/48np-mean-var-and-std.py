#Url: https://www.hackerrank.com/challenges/np-mean-var-and-std/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	n, m = list(map(int, input().split()))
	my_array = numpy.array([list(map(int, input().split())) for _ in range(n) ])
	numpy.set_printoptions(legacy='1.13')
	print(numpy.mean(my_array, axis = 1))
	print(numpy.var(my_array, axis = 0))
	print(numpy.std(my_array, axis = None))

# The python libraries may have set_printoptions to configure the 
# Print calibre for old libraries.
#Url: https://www.hackerrank.com/challenges/counting-valleys/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#
def countingValleys(steps, path):
	FDownF, valleys, temporal = True, 0, 0
	if path[0] == "U":
		FDownF = False
	for s in path:
		if s == "U":
			temporal += 1
		else:
			temporal -= 1
		if temporal == 0:
			if FDownF:
				valleys += 1
			else:
				FDownF = True
		if temporal == 1 and FDownF:
			FDownF = False
	return valleys

if __name__ == '__main__':
	steps = int(input().strip())

	path = input()

	print (countingValleys(steps, path))



#Url: https://www.hackerrank.com/challenges/np-concatenate/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	the_n_size, the_m_size, the_p_colums = list(map(int, input().split()))
	rowsn = [list(map(int, input().split())) for _ in range(the_n_size)] 
	rowsm = [list(map(int, input().split())) for _ in range(the_m_size)] 
	
	numpy.set_printoptions(legacy='1.13')
	array_1_for_rowsn = numpy.array(rowsn)
	array_2_for_rowsm = numpy.array(rowsm)

	print (numpy.concatenate((array_1_for_rowsn, array_2_for_rowsm), axis = 0))

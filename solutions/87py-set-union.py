#Url: https://www.hackerrank.com/challenges/py-set-union/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	size_set_a, the_a_set_l = int(input()), set(list(map(int, input().split())))
	size_set_b, the_b_set_l = int(input()), set(list(map(int, input().split())))
	print(len(the_a_set_l.intersection(the_b_set_l)))

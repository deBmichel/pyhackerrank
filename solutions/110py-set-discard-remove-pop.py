#Url: https://www.hackerrank.com/challenges/py-set-discard-remove-pop/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	input()
	numbers = set(list(map(int, input().split())))
	operations = int(input())
	for i in range (operations):
		operation = input().split()
		if operation[0] == "pop":
			numbers.pop()
		elif operation[0] == "remove":
			numbers.remove(int(operation[1]))
		elif operation[0] == "discard":
			numbers.discard(int(operation[1]))
	print(sum(numbers))
		

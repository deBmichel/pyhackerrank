#Url: https://www.hackerrank.com/challenges/the-minion-game/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
def minion_game(str):
	KEVINVALIDS = {'A', 'E', 'I', 'O', 'U'}
	index, flagindex = 0, len(str)
	kevinscore, stuarscore = 0, 0
	while index < flagindex:
		if str[index] in KEVINVALIDS:
			kevinscore += (flagindex - index)
		else:
			stuarscore += (flagindex - index)
		index += 1
	if stuarscore > kevinscore:
		print("Stuart", stuarscore)
	elif stuarscore < kevinscore:
		print("Kevin", kevinscore)
	else:
		print("Draw")
	
if __name__ == '__main__':
    s = input()
    minion_game(s)

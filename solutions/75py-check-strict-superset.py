#Url: https://www.hackerrank.com/challenges/py-check-strict-superset/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	main_set, adts_sets = set(list(map(int, input().split()))), int(input())
	verify = 0

	for i in range(adts_sets):
		a_set = set(list(map(int, input().split())))
		if a_set.intersection(main_set) == a_set:
			verify += 1

	if verify == adts_sets:
		print(True)
	else:
		print(False)

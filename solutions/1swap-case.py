#Url: https://www.hackerrank.com/challenges/swap-case/problem
#Python Python 3.8.2
#@Author: Michel M S 

def swap_case(s):
	strreturn = ""
	for str in s:
		if str.islower():
			strreturn += str.upper()
		elif str.isupper():
			strreturn += str.lower()
		else:
			strreturn += str
	return strreturn

if __name__ == '__main__':
	s = input()
	result = swap_case(s)
	print(result)
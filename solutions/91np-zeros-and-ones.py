#Url: https://www.hackerrank.com/challenges/np-zeros-and-ones/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	dims = tuple(list(map(int, input().split())))
	
	numpy.set_printoptions(legacy='1.13')
	print (numpy.zeros(dims, dtype = numpy.int))
	print (numpy.ones(dims, dtype = numpy.int))

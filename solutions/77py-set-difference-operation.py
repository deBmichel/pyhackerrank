#Url: https://www.hackerrank.com/challenges/py-set-difference-operation/problem?h 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	a_size_a, the_a_set_a = int(input()), set(list(map(int, input().split())))
	b_size_b, the_b_set_b = int(input()), set(list(map(int, input().split())))
	print(len(the_a_set_a.difference(the_b_set_b)))


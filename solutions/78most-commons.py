#Url: https://www.hackerrank.com/challenges/most-commons/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
from collections import Counter

if __name__ == "__main__":
	dict_cchars = Counter(input()).items()
	ordered = sorted( dict_cchars, key=lambda letter: (-letter[1], letter[0]) ) [:3]
	for letter, times in ordered:
		print(letter, times)
		

		#I must to understand the lamda key in sorted
		
#Url: https://www.hackerrank.com/challenges/compress-the-string/problem 
#Python Python 3.8.2 
#@Author: Michel M S 


from itertools import groupby

# https://docs.python.org/3/library/itertools.html#itertools.groupby

if __name__ == "__main__":
	str = input()
	# I should pay more attention to the documentation comments
	group = groupby(str)
	for val, l in group:
		print (f'({len(list(l))}, {int(val)})', end=" ")
	print()
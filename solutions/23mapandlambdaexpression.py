#Url: https://www.hackerrank.com/challenges/map-and-lambda-expression/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

cube = lambda x: x**3 

def gen(amount):
    index, base, prev, flagbase = 0, 0, 0, True
    while index < amount:
        yield base
        if not flagbase:
            tem = base
            base += prev
            prev = tem
        else:
            flagbase = False
            base = 1
            prev = 0
        index += 1

def fibonacci(n):
    return [i for i in gen(n)]

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))
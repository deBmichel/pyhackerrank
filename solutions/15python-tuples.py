#Url: https://www.hackerrank.com/challenges/python-tuples/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == '__main__':
    n = int(input())
    integer_list = input().split()
    input_list = [int(x) for x in integer_list]
    t = tuple(input_list)
    print(hash(t))

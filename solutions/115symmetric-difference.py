#Url: https://www.hackerrank.com/challenges/symmetric-difference/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	input()
	a = set(list(map(int, input().split())))
	input()
	b = set(list(map(int, input().split())))
	c = list( a.difference(b)) + list( b.difference(a))
	c.sort()
	for e in c:
		print(e)
#Url: https://www.hackerrank.com/challenges/electronics-shop/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

#!/bin/python3

import os
import sys

#
# Complete the getMoneySpent function below.
#
def getMoneySpent(keyboards, drives, b):
	from collections import deque
	keyboards.sort()
	drives.sort()
	lk = list(dict.fromkeys(keyboards))
	ld = list(dict.fromkeys(drives))
	if lk[0] + ld[0] > b:
		return -1

	if len(lk) > len(ld):
		downing, uppingflag, upL, doL = len(lk)-1, len(ld), ld, lk
	else:
		downing, uppingflag, upL, doL = len(ld)-1, len(lk), lk, ld
	results = deque()
	while downing > 0:
		upping = 0
		while upping < uppingflag:
			if upL[upping] + doL[downing] <= b:
				results.append(upL[upping] + doL[downing])
			else:
				break
			upping += 1
		downing -= 1
	return max(results)
	
if __name__ == '__main__':
    bnm = input().split()

    b = int(bnm[0])

    n = int(bnm[1])

    m = int(bnm[2])

    keyboards = list(map(int, input().rstrip().split()))

    drives = list(map(int, input().rstrip().split()))

    #
    # The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
    #

    print(getMoneySpent(keyboards, drives, b))

    
#Url: https://www.hackerrank.com/challenges/re-start-re-end/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import re

if __name__ == "__main__":
	the_complete_s, the_sub_v = input(), input()

	for element, _ in enumerate(the_complete_s):
	    if re.match(the_sub_v,the_complete_s[element:]):
	        print((element, element + len(the_sub_v) - 1))

	if re.search(the_sub_v, the_complete_s) is None:
	    print("(-1, -1)") 
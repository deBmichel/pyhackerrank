#Url: https://www.hackerrank.com/challenges/py-set-intersection-operation/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	size_a, the_a_set = int(input()), set(list(map(int, input().split())))
	size_b, the_b_set = int(input()), set(list(map(int, input().split())))
	print(len(the_a_set.intersection(the_b_set)))
			
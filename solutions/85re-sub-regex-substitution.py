#Url: https://www.hackerrank.com/challenges/re-sub-regex-substitution/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
replacer = lambda thestr: thestr.replace(" && ", " and ").replace(" || ", " or ")

if __name__=="__main__":
	codelines = int(input())
	for _ in range(codelines):
	    code = input()
	    while ' && ' in code or ' || ' in code:
	        code = replacer(code)
	    print(code)


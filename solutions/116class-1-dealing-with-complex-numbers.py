import math

#The important lesson was overwrite operators is interesting, 
#I should improve my english to avoid tactical complications.

class Complex:
    def __init__(self, real, imaginary):
        self.real=real
        self.imaginary=imaginary

    def __add__(self, no):
        return Complex(self.real+no.real,self.imaginary+no.imaginary)
        
    def __sub__(self, no):
        return Complex(self.real-no.real,self.imaginary-no.imaginary)
        
    def __mul__(self, no):
        return Complex(self.real*no.real-self.imaginary*no.imaginary,self.real*no.imaginary+self.imaginary*no.real)

    #IN PYTHON 3.X is not Div, is truediv
    def __truediv__(self, no):
        u=(no.real**2+no.imaginary**2)
        return Complex((self.real*no.real+self.imaginary*no.imaginary)/u,(self.imaginary*no.real-self.real*no.imaginary)/u)

    def mod(self):
        c=(self.real**2+self.imaginary**2)
        return Complex(c/c**.5,00)

    def __str__(self):
        return '{0.real:.2f}{0.imaginary:+.2f}i'.format(self)



if __name__ == '__main__':
    c = map(float, input().split())
    d = map(float, input().split())
    x = Complex(*c)
    y = Complex(*d)
    print(*map(str, [x+y, x-y, x*y, x/y, x.mod(), y.mod()]), sep='\n')
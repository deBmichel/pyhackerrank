#Url: https://www.hackerrank.com/challenges/nested-list/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
if __name__ == '__main__':
	TheDict = {} 
	for _ in range(int(input())):
		name = input()
		score = float(input())
		if score in TheDict:
			prev = TheDict[score]
			prev.append(name)
			prev.sort()
			TheDict[score] = prev
		else:
			TheDict[score] = [name,]

	finallist = list(TheDict.keys())
	finallist.sort()
	finallist = finallist[1]

	printablelist = TheDict[finallist]
	i, lenlist = 0, len(printablelist)
	while i < lenlist:
		print(printablelist[i])
		i+=1

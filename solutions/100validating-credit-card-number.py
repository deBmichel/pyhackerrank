#Url: https://www.hackerrank.com/challenges/validating-credit-card-number/problem?h 
#Python Python 3.8.2 
#@Author: Michel M S 

import re

matcher_base = lambda CardNUmber: re.match(r"^[456]([\d]{15}|[\d]{3}(-[\d]{4}){3})$", CardNUmber)
matcher_number = lambda CardNUmber: not re.search(r"([\d])\1\1\1", CardNUmber)

if __name__ == "__main__":
	cases = int(input())
	for _ in range(cases):
		str_CardNUmber = input()
		sub_CardNUmber = str_CardNUmber.replace("-", "")
		if matcher_base(str_CardNUmber) and matcher_number(sub_CardNUmber):
			print("Valid")
		else:
			print("Invalid")
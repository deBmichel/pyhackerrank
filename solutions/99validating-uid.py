#Url: https://www.hackerrank.com/challenges/validating-uid/problem?h 
#Python Python 3.8.2 
#@Author: Michel M S 

import re

options = [
	lambda UIDD : re.search(r'[A-Z]{2}', UIDD),
	lambda UIDD : re.search(r'\d\d\d', UIDD),
	lambda UIDD : not re.search(r'[^a-zA-Z0-9]', UIDD),
	lambda UIDD : not re.search(r'(.)\1', UIDD),
	lambda UIDD : len(UIDD) == 10,
]

if __name__ == "__main__":
	for _ in range(int(input())):
		UIDD = ''.join(sorted(input()))
		try:
			assert all([e(UIDD) for e in options])
		except:
			print('Invalid')
		else:
			print('Valid')
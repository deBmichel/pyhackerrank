#Url: https://www.hackerrank.com/challenges/itertools-combinations/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from itertools import combinations

def get_combination(word, k):
	newlist = [e for e in word]
	newlist.sort()
	
	Combs = list(combinations(newlist,k))
	Combs.sort()
	return Combs

if __name__ == "__main__":
	largs = input().split()
	word, K, ki = largs[0], int(largs[1]), 0
	while ki < K:
		combs = get_combination(word, ki+1)
		i, flagindex = 0, len(combs)
		while i < flagindex:
			print("".join(combs[i]))
			i += 1
		ki += 1


"""
	The line 8 is very especial you can not
	call combinations or permutations consecutive
	without using a extra funcion.

	A    A
	C    C
	H    H
	K    K
	AC   AC
	AK   AH
	CK   AK
	HA   CH
	HC   CK
	HK   HK















			"""
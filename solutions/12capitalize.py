#Url: https://www.hackerrank.com/challenges/capitalize/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

# Moraleja : Be carefull with translate function, and to less review the first case of
# probes

#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the solve function below.
def solve(s):
	res = re.findall(r"\s{1}[^ ]", s) + re.findall(r"\t{1}[^\t]", s)
	mylist = list(dict.fromkeys(res))
	
	strret = s[0].upper() + s[1:]
	#strret = s
	for change in mylist:
		strret = strret.replace(change, change.upper())

	return strret

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = solve(s)

    fptr.write(result + '\n')

    fptr.close()

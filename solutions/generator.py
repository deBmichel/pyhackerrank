#!/usr/bin/python3
import sys, os

basename = sys.argv[2].split("/")[4]
consecutive = sys.argv[1]
filename = consecutive + basename + ".py"
command = f'''echo '#Url: {sys.argv[2]} \n#Python Python 3.8.2 \n#@Author: Michel M S ' > {filename}'''
os.system(command)
print("Pygenerator created:", filename)
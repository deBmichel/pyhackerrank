#Url: https://www.hackerrank.com/challenges/drawing-book/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import os
import sys

#
# Maths are powerfull but I need be carefull with lecture and
# indications.
#
def pageCount(n, p):
    return min((p//2), (n//2-p//2))

if __name__ == '__main__':
    n = int(input())

    p = int(input())

    print(pageCount(n, p))

#Url: https://www.hackerrank.com/challenges/exceptions/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	cases = int(input())
	for _ in range(cases):
		try:
			val_a, val_b = list(map(int, input().split()))
			print(val_a // val_b)
		except Exception as eCode:
			print ("Error Code:", eCode)

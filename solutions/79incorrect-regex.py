#Url: https://www.hackerrank.com/challenges/incorrect-regex/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import re

if __name__=="__main__":
	for i in range(int(input())):
		try:
			re.compile(input())
			print(True)
		except re.error:
			print(False)

# A great page about regular expressions is https://en.wikipedia.org/wiki/Regular_expression
# I found a lot of example in this page.
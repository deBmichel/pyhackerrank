#Url: https://www.hackerrank.com/challenges/np-shape-reshape/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
import numpy

if __name__ == "__main__":
	the_array = numpy.array(list(map(int, input().split())))
	numpy.set_printoptions(legacy='1.13')
	print (numpy.reshape(the_array,(3,3)))
#Url: https://www.hackerrank.com/challenges/itertools-product/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from itertools import product

if __name__ == "__main__":
	a = list(map(int, input().split(" ")))
	b = list(map(int, input().split(" ")))
	p = list(product(a,b))
	i, flagindex = 0, len(p)
	while i < flagindex-1:
		print(p[i], end = ' ')
		i += 1
	print(p[-1])



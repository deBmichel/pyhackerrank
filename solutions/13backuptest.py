#Url: https://www.hackerrank.com/challenges/the-minion-game/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
import re

def get_uprstr_chrs_for_player(uperstr, playervalids):
	structset = set(uperstr) & playervalids
	return structset

def find_uprstrchrs_for_player(uperstr, playervalchrs):
	retlist = list()
	for c in playervalchrs:
		allindexes = [m.start() for m in re.finditer(c, uperstr)]
		for charindex in allindexes:
			retlist.append(charindex)
	return retlist

def count_subword_in_uprstr(substrslist, uperstr):
	total = 0
	for substr in substrslist:
		total += len([m.start() for m in re.finditer(f'(?={substr})', uperstr)])
	return total

def obtain_substrs_usngixchr(charindexes, uperstr):
	substrs = list()
	for index in charindexes:
		for i in range(len(uperstr), index-1, -1):
			newsubstr = uperstr[index:i]
			if not newsubstr in substrs and newsubstr != "":
				substrs.append(newsubstr)
	return substrs

def determine_player_score(str, playercharoptions):
	playeroptions = get_uprstr_chrs_for_player(str, playercharoptions)
	playerindexes = find_uprstrchrs_for_player(str, playeroptions)
	wordsforplayer = obtain_substrs_usngixchr(playerindexes, str)
	return count_subword_in_uprstr(wordsforplayer, str)

def minion_game(string):
	#I think in phantoms ....
	str = string.upper()

	STUARVALIDS = {
		'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 
		'S', 'T', 'V', 'W', 'X', 'Y', 'Z'
	}

	KEVINVALIDS = {'A', 'E', 'I', 'O', 'U'}

	import datetime
	start = datetime.datetime.now()

	kevinscore = determine_player_score(str, KEVINVALIDS)
	stuarscore = determine_player_score(str, STUARVALIDS)
	if stuarscore > kevinscore:
		print("Stuart", stuarscore)
	elif stuarscore < kevinscore:
		print("Kevin", kevinscore)
	else:
		#Remeber to read ...
		print("Draw")
	finish = datetime.datetime.now()
	print (finish-start)

if __name__ == '__main__':
    s = input()
    minion_game(s)

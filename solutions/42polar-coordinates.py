#Url: https://www.hackerrank.com/challenges/polar-coordinates/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from cmath import phase

if __name__ == "__main__":
	line = input()
	args = line.split(f'+')
	flag = 1
	if len(args) > 1:
		x,y = args
	else:
		flag = len(line.split(f'-'))
		x,y = [e for e in line.split(f'-') if e != ""]
	y = y.split("j")[0]
	print (f"{abs(complex(float(x), float(y))):.4f}")
	if flag == 3: 
		print (f"{phase(complex(float(x)*-1, float(y)*-1)):.4f}")
	elif flag == 2:
		print (f"{phase(complex(float(x)*-1, float(y))):.4f}")
	else:
		print (f"{phase(complex(float(x), float(y))):.4f}")
## COmplex and abs are by default you dont have nothing to do-
## It's great.
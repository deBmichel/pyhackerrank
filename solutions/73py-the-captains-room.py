#Url: https://www.hackerrank.com/challenges/py-the-captains-room/problem 
#Python Python 3.8.2 
#@Author: Michel M S

if __name__ == "__main__":
	k, the_rooms = int(input()),list(map(int, input().split()))
	rooms_set = set(the_rooms)
	#Maths are pure beauty ......
	print(((sum(rooms_set)*k) - (sum(the_rooms)))//(k-1))

	#Everybody should review this code implementation and the main answer in the 
	#forum, maths give a lot of ultra speed
	
	

#Url: https://www.hackerrank.com/challenges/np-linear-algebra/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
import numpy

if __name__ == "__main__":
	print(f"{round(numpy.linalg.det([list(map(float, input().split())) for _ in range(int(input()))]), 2)}")


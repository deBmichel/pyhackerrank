#Url: https://www.hackerrank.com/challenges/alphabet-rangoli/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
def create_base(size):
	strret = ""
	for i in range(96+n, 96, -1):
		strret += chr(i) + "-"
	for i in range(98, 96+n+1, 1):
		strret += chr(i) + "-"
	return strret[0:-1]

def create_mirror_a(base,size):
	retlist, tem = list(), base[0:(len(base)//2)-2]+base[(len(base)//2-3)+5:]
	tem = tem.center(len(base),"-")
	for i in range(size-1,0,-1):
		aux = tem[0:(len(tem)//2)-2]+tem[(len(tem)//2-3)+5:]
		aux = aux.center(len(base),"-")
		retlist.append(tem)
		tem = aux
	return retlist

def print_rangoli(size):
	base = create_base(size)
	mirror = create_mirror_a(base,size)
	for i in range(len(mirror)-1, -1, -1):
		print(mirror[i])
	print(base)
	for i in mirror:
		print(i)

if __name__ == '__main__':
    n = int(input())
    print_rangoli(n)

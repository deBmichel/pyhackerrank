#Url: https://www.hackerrank.com/challenges/py-collections-deque/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
#REview:
#	https://docs.python.org/2.7/library/collections.html#deque-recipes
#   https://docs.python.org/2/library/collections.html#deque-objects
from collections import deque

if __name__ == "__main__":
	d = deque()
	ordrs, i = int(input()), 0
	while i < ordrs:
		ordr = input().split(" ")
		if ordr[0] == "append":
			d.append(ordr[1])
		elif ordr[0] == "appendleft":
			d.appendleft(ordr[1])
		elif ordr[0] == "pop":
			d.pop()
		elif ordr[0] == "popleft":
			d.popleft()
		i+=1
	print (" ".join(d))
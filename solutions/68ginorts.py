#Url: https://www.hackerrank.com/challenges/ginorts/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__=="__main__":
	s, i, lowrS, upprS, numbrSo, numbrSe = input(), 0, [], [], [], []
	while i < len(s):
		if s[i] >='a' and s[i] <='z':
			lowrS.append(s[i])
		elif s[i] >='A' and s[i] <='Z':
			upprS.append(s[i])
		elif s[i]=='1' or s[i]=='3' or s[i]=='5' or s[i]=='7' or s[i]=='9':
			numbrSo.append(s[i])
		else:
			numbrSe.append(s[i])
		i += 1

	lowrS.sort()
	upprS.sort()
	numbrSo.sort()
	numbrSe.sort()

	print("".join(lowrS + upprS + numbrSo + numbrSe))

"""I should review this 
print(*sorted(input(), key=lambda c: (-ord(c) >> 5, c in '02468', c)), sep='')

print(*sorted(input(), key=lambda c: (c.isdigit() - c.islower(), c in '02468', c)), sep='')

order = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1357902468'
print(*sorted(input(), key=order.index), sep='')

import string
print(*sorted(input(), key=(string.ascii_letters + '1357902468').index), sep='')

"""
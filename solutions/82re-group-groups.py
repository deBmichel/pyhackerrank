#Url: https://www.hackerrank.com/challenges/re-group-groups/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
import re

if __name__ == "__main__":
	thegroupsregex = re.search(r"([a-z0-9])\1+", input())
	print(thegroupsregex.group(1) if thegroupsregex else -1)





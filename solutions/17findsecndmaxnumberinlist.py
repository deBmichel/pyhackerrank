#Url: https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == '__main__':
    n = int(input())
    arr = [int(stri) for stri in input().split()]
    arr.sort()
    arr = list(dict.fromkeys(arr))
    print(arr[-2])


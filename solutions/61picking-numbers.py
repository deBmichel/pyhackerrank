#Url: https://www.hackerrank.com/challenges/picking-numbers/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

#
# Complete the 'pickingNumbers' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#

def pickingNumbers(a):
    base, toreturn = 1, 0
    i = 0
    theDict = dict()
    globallst = []
    tem = []
    tem.append(a[0])
    while i < len(a)-1:
    	if abs(a[i]-a[i+1]) == 1 or ((a[i]-a[i+1]) == 0):
    		if not a[i] in tem:
    			tem.append(a[i])	
    		tem.append(a[i+1])
    	else:
    		globallst.append(tem)
    		tem = []
    		tem.append(a[i+1])
    	i += 1

    globallst.append(tem)
    print(globallst)
    
    return toreturn

if __name__ == '__main__':
    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    print(pickingNumbers(a))






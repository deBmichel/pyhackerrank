#Url: https://www.hackerrank.com/challenges/maximize-it/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

lmd_square = lambda x: x**2

def calc_smax(numbers, M):
	squares = map(lmd_square, numbers)
	return sum(squares) % M

if __name__=="__main__":
	vals = list(map(int, input().split()))
	list_ammount, M, i, Mayor = vals[0], vals[1], 0, 0
	lists = list()
	while i < list_ammount:
		l = list(map(int, input().split()))
		l.sort()
		lists.append(l)
		i += 1

	combinations, i = [], 1

	temlistcero, flagtemlistcero = 0, len(lists[0])
	while temlistcero < flagtemlistcero:
		tl = []
		tl.append(lists[0][temlistcero])
		combinations.append(tl)
		temlistcero += 1

	while i < list_ammount:
		listi, listiix = lists[i], 0
		listiflag = len(listi)
		charge = []
		while listiix < listiflag:
			base = [list(tuple(e)) for e in combinations]
			baseix, baseflag = 0, len(base)
			while baseix < baseflag:
				base[baseix].append(listi[listiix])
				baseix += 1
			temibase, flagtemibase = 0, len(base)
			while temibase < flagtemibase:
				charge.append(base[temibase])
				temibase += 1
			listiix += 1
		combinations = []
		ixcharge, flagixcharge = 0, len(charge)
		while ixcharge < flagixcharge:
			combinations.append(charge[ixcharge])
			ixcharge += 1
		i += 1

	i, iflag = 0, len(combinations)
	tem = []
	while i < iflag:
		valcal = calc_smax(combinations[i], M)
		if valcal >= Mayor:
			Mayor = valcal
			tem = combinations[i]
		i += 1		

	print (Mayor, tem)

	from itertools import product

	K,M = map(int,input().split())
	N = (list(map(int, input().split()))[1:] for _ in range(K))
	results = map(lambda x: sum(i**2 for i in x)%M, product(*N))
	print(max(results))

"""

from itertools import product

def f1():
	return lambda POW: int(POW) ** 2

def f2():
	return lambda MODULE: sum(MODULE) % M

if __name__=="__main__":
	list_ammount, M = map(int, input().split())
	aclist = list()

	for i in range(list_ammount):
	    b = list(map(f1(), input().split()[1:]))
	    aclist.append(b)

	MAX = max(map(f2(), product(*aclist)))

	print(MAX)
"""
"""
3 1000
2 5 4         
3 7 8 9       
5 7 8 9 10    



****.
s***.
.***s
.****
..s*s


eeeAAAAAA
eeAAAAAAA
eAeAAAAAA
AAAAAAAAA
AAAAAAAAA
AAAAAAAAA
AAAAAAAAA





1





6 767
2 488512261 423332742
2 625040505 443232774
1 4553600
4 92134264 617699202 124100179 337650738
2 778493847 932097163
5 489894997 496724555 693361712 935903331 518538304

 [
		 488512261, from str(1)  
		 625040505, from str(2)  
		 1        , from str(3) 
		 617699202, from str(4)  
		 778493847, from str(5)  
		 935903331, from str(5)
  ]


>>> [res.append(x) for x in retcmbs if x not in res] 


[[2, 3, 5], 
[2, 3, 7], 
[2, 3, 8], 
[2, 3, 9], 
[2, 3, 10], [2, 4, 3], [2, 4, 5], [2, 4, 7], [2, 4, 8], [2, 4, 9], [2, 4, 10], [2, 5, 3], [2, 5, 4], [2, 5, 5], [2, 5, 7], [2, 5, 8], [2, 5, 9], [2, 5, 10], [2, 7, 5], [2, 7, 7], [2, 7, 8], [2, 7, 9], [2, 7, 10], [2, 8, 5], [2, 8, 7], [2, 8, 8], [2, 8, 9], [2, 8, 10], [2, 9, 5], [2, 9, 7], [2, 9, 8], [2, 9, 9], [2, 9, 10], [3, 5, 7], [3, 5, 8], [3, 5, 9], [3, 5, 10], [3, 7, 5], [3, 7, 7], [3, 7, 8], [3, 7, 9], [3, 7, 10], [3, 8, 5], [3, 8, 7], [3, 8, 8], [3, 8, 9], [3, 8, 10], [3, 9, 5], [3, 9, 7], [3, 9, 8], [3, 9, 9], [3, 9, 10], [4, 3, 5], [4, 3, 7], [4, 3, 8], [4, 3, 9], [4, 3, 10], [4, 5, 7], [4, 5, 8], [4, 5, 9], [4, 5, 10], [4, 7, 5], [4, 7, 7], [4, 7, 8], [4, 7, 9], [4, 7, 10], [4, 8, 5], [4, 8, 7], [4, 8, 8], [4, 8, 9], [4, 8, 10], [4, 9, 5], [4, 9, 7], [4, 9, 8], [4, 9, 9], [4, 9, 10], [5, 3, 5], [5, 3, 7], [5, 3, 8], [5, 3, 9], [5, 3, 10], [5, 4, 3], [5, 4, 5], [5, 4, 7], [5, 4, 8], [5, 4, 9], [5, 4, 10], [5, 5, 7], [5, 5, 8], [5, 5, 9], [5, 5, 10], [5, 7, 5], [5, 7, 7], [5, 7, 8], [5, 7, 9], [5, 7, 10], [5, 8, 5], [5, 8, 7], [5, 8, 8], [5, 8, 9], [5, 8, 10], [5, 9, 5], [5, 9, 7], [5, 9, 8], [5, 9, 9], [5, 9, 10], [7, 5, 7], [7, 5, 8], [7, 5, 9], [7, 5, 10], [7, 7, 8], [7, 7, 9], [7, 7, 10], [7, 8, 5], [7, 8, 7], [7, 8, 8], [7, 8, 9], [7, 8, 10], [7, 9, 5], [7, 9, 7], [7, 9, 8], [7, 9, 9], [7, 9, 10], [8, 5, 7], [8, 5, 8], [8, 5, 9], [8, 5, 10], [8, 7, 8], [8, 7, 9], [8, 7, 10], [8, 8, 9], [8, 8, 10], [8, 9, 5], [8, 9, 7], [8, 9, 8], [8, 9, 9], [8, 9, 10], [9, 5, 7], [9, 5, 8], [9, 5, 9], [9, 5, 10], [9, 7, 8], [9, 7, 9], [9, 7, 10], [9, 8, 9], [9, 8, 10], [9, 9, 10]]

  


6 767
2 488512261 423332742
2 625040505 443232774

1 4553600
4 92134264 617699202 124100179 337650738
2 778493847 932097163
5 489894997 496724555 693361712 935903331 518538304

2 2
2 625040505
2 443232774
488512261 2
488512261 625040505 
488512261 443232774 
423332742 2
423332742 625040505
423332742 443232774

(2, 2), 
(2, 423332742), 
(2, 443232774), (2, 488512261), (2, 625040505), (423332742, 2), (423332742, 423332742), (423332742, 443232774), (423332742, 488512261), (423332742, 625040505), (443232774, 443232774), (443232774, 625040505), (488512261, 2), (488512261, 443232774), (488512261, 488512261), (488512261, 625040505), (625040505, 625040505)




2 3 
2 7 
2 8 
2 9 

5 3 
5 7 
5 8 
5 9 

4 3 
4 7 
4 8 
4 9 

iALLlists, iALLlistsflag = 1, len(lists)
	while iALLlists < iALLlistsflag:
		singlelist = lists[iALLlists]
		ilist, ilistflag = 1, len(singlelist)
		while ilist < ilistflag:
			flagcombi = len(combs)
			while combi < flagcombi:
				print("combs[combi]", combs[combi], "singlelist[ilist]", singlelist[ilist])
				combs[combi].append(singlelist[ilist])
				combs[combi] = get_ret_comb(combs[combi], iALLlists + 1)
				combi += 1
			ilist += 1	
		iALLlists += 1
	



"""











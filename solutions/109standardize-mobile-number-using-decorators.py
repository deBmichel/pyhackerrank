#Url: https://www.hackerrank.com/challenges/standardize-mobile-number-using-decorators/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def wrapper(f):
    def fun(l):
        # I Like format and is the special election for this case
        f('+91 {} {}'.format(number[-10:-5], number[-5:]) for number in l)
    return fun

@wrapper
def sort_phone(l):
    print(*sorted(l), sep='\n')

if __name__ == '__main__':
    l = [input() for _ in range(int(input()))]
    sort_phone(l) 



#Url: https://www.hackerrank.com/challenges/python-integers-come-in-all-sizes/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
if __name__ == '__main__':
	a, b, c, d = (int(input()) for _ in range(4))
	print((a**b) + (c**d))



#Url: https://www.hackerrank.com/challenges/xml2-find-the-maximum-depth/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import xml.etree.ElementTree as etree

maxdepth = 0
def depth(elem, level):
	# Global is criticable and a bad practice here 
    global maxdepth
    # GOod new , global is a reserve python word to create variables in the main
    # context, but pure poison, global is not good practice.
    # Implementing a proto copy of the classic problem 
    # factorial of n number https://www.sanfoundry.com/python-program-find-factorial-number-recursion/
    level += 1
    if level >= maxdepth:
        maxdepth = level
    for child in elem:
        depth(child, level)

if __name__ == '__main__':
    n = int(input())
    xml = ""
    for i in range(n):
        xml =  xml + input() + "\n"
    tree = etree.ElementTree(etree.fromstring(xml))
    depth(tree.getroot(), -1)
    print(maxdepth)
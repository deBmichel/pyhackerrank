#Url: https://www.hackerrank.com/challenges/iterables-and-iterators/problem 
#Python Python 3.8.2 
#@Author: Michel M S

from itertools import combinations
import re

def get_combination(indexes, k):
	Combs = list(combinations(indexes,k))
	Combs.sort()
	return Combs

if __name__ == "__main__":
	size, lnchars, K = int(input()), "".join(input().split()), int(input())
	indexes = [e+1 for e in range(size)]
	combs = get_combination(indexes, K)
	a_matches = re.finditer("a", lnchars)
	a_indexes = [match.start() + 1 for match in a_matches]
	ocurrences, elements = 0, len(combs)
	for e in combs:
		foundiine = False
		for i in a_indexes:
			if foundiine:
				break
			for ec in e:
				if ec == i:
					ocurrences += 1
					foundiine = True
					break
	print (f"{float(ocurrences / elements):.4f}")

#Url: https://www.hackerrank.com/challenges/finding-the-percentage/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        l = list(map(float, line))
        student_marks[name] = float((l[0] + l[1] + l[2]) / 3)
    query_name = input()
    #Formating decimal ?
    print (f"{student_marks[query_name]:.2f}")


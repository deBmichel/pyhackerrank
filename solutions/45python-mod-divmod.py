#Url: https://www.hackerrank.com/challenges/python-mod-divmod/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	n, d = int(input()), int(input())
	print (f'{n // d}\n{n % d}\n{divmod(n, d)}')


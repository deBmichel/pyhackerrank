#Url: https://www.hackerrank.com/challenges/matrix-script/problem 
#Python Python 3.8.2 
#@Author: Michel M S
"""
import numpy
import re

if __name__ == "__main__" :
	first_multiple_input = input().rstrip().split()
	n = int(first_multiple_input[0])
	m = int(first_multiple_input[1])
	matrix = []

	for _ in range(n):
		matrix_item = [e for e in input()]
		matrix.append(matrix_item)

	the_matrix = numpy.array(matrix)
	the_matrix = "".join(numpy.transpose(the_matrix).ravel())
	
	print(re.sub(r"(?<=\w)([^\w]+)(?=\w)", " ", the_matrix))
"""
import re 

#What is the problem with the main and numpy ??

first_multiple_input = input().rstrip().split()
n = int(first_multiple_input[0])
m = int(first_multiple_input[1])
matrix = []

for _ in range(n):
    matrix_item = input()
    matrix.append(matrix_item)

lineal_str = ""
for z in zip(*matrix):
    lineal_str += "".join(z)

print(re.sub(r"(?<=\w)([^\w]+)(?=\w)", ' ', lineal_str))










#Url: https://www.hackerrank.com/challenges/re-split/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

regex_pattern = r"[\.|,]"	# Do not delete 'r'.

import re

print("\n".join(re.split(regex_pattern, input())))
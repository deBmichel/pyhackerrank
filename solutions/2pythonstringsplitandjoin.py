#Url:https://www.hackerrank.com/challenges/python-string-split-and-join/problem
#Python Python 3.8.2
#@Author: Michel M S 

def split_and_join(line):
    return "-".join(line.split(" "))

if __name__ == '__main__':
    line = input()
    result = split_and_join(line)
    print(result)
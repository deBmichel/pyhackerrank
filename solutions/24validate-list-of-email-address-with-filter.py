#Url: https://www.hackerrank.com/challenges/validate-list-of-email-address-with-filter/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def fun(s):
    list = s.split("@")
    if len(list) == 2 and list[0] != "":
    	username = list[0]
    	for i in username:
    		if i.isalpha() or i.isdigit() or (i == "_") or (i == "-"):
    			continue
    		else:
    			return False
    	list = list[1].split(".")
    	if len(list) == 2:
    		if list[0].isalnum():
    			if len(list[1]) <= 3:
    				return True
    			else:
    				return False
    		else:
    			return False
    	else:
    		return False
    else:
    	return False

def filter_mail(emails):
    return list(filter(fun, emails))

if __name__ == '__main__':
    n = int(input())
    emails = []
    for _ in range(n):
        emails.append(input())

filtered_emails = filter_mail(emails)
filtered_emails.sort()
print(filtered_emails)
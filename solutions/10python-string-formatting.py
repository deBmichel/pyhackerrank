#Url: https://www.hackerrank.com/challenges/python-string-formatting/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
get_dec_rep = lambda inti: str(inti)

get_oct_rep = lambda inti: str(oct(inti).split("o")[1])

get_dec_hex = lambda inti: str(hex(inti).split("x")[1]).upper()

get_bin = lambda x, n: format(x, 'b').zfill(n)
get_bin_rep = lambda strrep: strrep[strrep.find("1"):]

def determinespaces(repnum, maxlenforrep):
	spaces_amount = maxlenforrep - len(repnum)
	return (" " * spaces_amount) + repnum

def maxwidths(number):
	decimalrep = get_dec_rep(number)
	octalrep = get_oct_rep(number)
	hexadrep = get_dec_hex(number)
	binaryrep = get_bin_rep (get_bin(number, 7))
	return len(binaryrep), (len(decimalrep+octalrep+hexadrep+binaryrep) * 2) + 1

def print_formatted(number):
	maxlenforrep, maxwidth = maxwidths(number)
	strret = ""
	
	for i in range(1, number+1):
		temline = ""

		decimrep = get_dec_rep(i)
		octalrep = get_oct_rep(i)
		hexadrep = get_dec_hex(i)
		binarrep = get_bin_rep (get_bin(i, 7))

		temline += determinespaces(decimrep, maxlenforrep) + " "
		temline += determinespaces(octalrep, maxlenforrep) + " "
		temline += determinespaces(hexadrep, maxlenforrep) + " "
		temline += determinespaces(binarrep, maxlenforrep)

		print(temline)

if __name__ == '__main__':
	n = int(input())
	print_formatted(n)


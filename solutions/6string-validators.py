#Url: https://www.hackerrank.com/challenges/string-validators/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def determine_string_props(str):
	results, flagTrue = [False,False,False,False,False], [True * 5]
	for s in str:
		if s.islower():
			results[3] = True
			results[1] = True
			results[0] = True

		if s.isupper():
			results[4] = True
			results[1] = True
			results[0] = True
			
		if s.isdigit():
			results[2] = True
			results[0] = True
			
		if (results[2] and results[3]) or (results[2] and results[4]):
			results[0] = True

	for result in results:
		print(result)
			
if __name__ == '__main__':
    str = input()
    determine_string_props(str)

#Url: https://www.hackerrank.com/challenges/python-power-mod-power/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == '__main__':
    a, b, m = (int(input()) for _ in range(3))
    print(a**b)
    print(pow(a,b,m))
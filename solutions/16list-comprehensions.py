#Url: https://www.hackerrank.com/challenges/list-comprehensions/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
def gen(limit):
	for x in range(limit+1):
		yield x

if __name__ == '__main__':
    x, y, z, n = (int(input()) for _ in gen(3))
    #Is posible to solve range on range.
    print([[a,b,c] for a in gen(x) for b in gen(y) for c in gen(z) if a + b + c != n])



#Url: https://www.hackerrank.com/challenges/word-order/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from collections import OrderedDict

if __name__ == '__main__':
	words, i, totalwords, = int(input()), 0, 0
	ordrd_words = OrderedDict()
	while i < words:
		word = input()
		if word in ordrd_words:	
			ordrd_words[word] = ordrd_words[word] + 1
		else:
			ordrd_words[word] = 1
			totalwords += 1
		i += 1
	print(totalwords)
	values = map(str, ordrd_words.values())
	print (" ".join(values))

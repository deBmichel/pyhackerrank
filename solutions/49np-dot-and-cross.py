#Url: https://www.hackerrank.com/challenges/np-dot-and-cross/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	ammount = int(input())
	the_matrixa = numpy.array([list(map(int, input().split())) for _ in range (ammount)])
	the_matrixb = numpy.array([list(map(int, input().split())) for _ in range (ammount)])
	print (the_matrixa.dot(the_matrixb))
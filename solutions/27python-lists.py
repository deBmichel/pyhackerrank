#Url: https://www.hackerrank.com/challenges/python-lists/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == '__main__':
	ordrs, i = int(input()), 0
	thelist = []
	while i < ordrs:
		ordr = input().split(" ")
		if ordr[0] == "print":
			print(thelist)

		elif ordr[0] == "pop":
			thelist.pop()

		elif ordr[0] == "reverse":
			thelist.reverse()

		elif ordr[0] == "sort":
			thelist.sort()

		elif ordr[0] == "append":
			thelist.append(int(ordr[1]))

		elif ordr[0] == "remove":
			thelist.remove(int(ordr[1]))

		elif ordr[0] == "insert":
			thelist.insert(int(ordr[1]), int(ordr[2]))
		i+=1





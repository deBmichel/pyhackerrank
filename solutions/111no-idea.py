#Url: https://www.hackerrank.com/challenges/no-idea/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

#It is not very pythonic but is really functional:
# Amazing lesson: using the in word onthe python set....
"""
	
	n, m = raw_input().split()

	sc_ar = raw_input().split()

	A = set(raw_input().split())
	B = set(raw_input().split())
	print sum([(i in A) - (i in B) for i in sc_ar])


	>>> False + True
	1
"""
if __name__ == "__main__":
	input()
	the_array = list(map(int, input().split()))
	A = set(list(map(int, input().split())))
	B = set(list(map(int, input().split())))
	hapinnes = 0
	for e in the_array:
		if e in A:
			hapinnes += 1
			continue
		if e in B:
			hapinnes -= 1
			continue
	print(hapinnes)
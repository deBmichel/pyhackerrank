#Url: https://www.hackerrank.com/challenges/re-findall-re-finditer/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

import re

def the_regex_expression():
	consonants, vocals = "qwrtypsdfghjklzxcvbnm", "aeiou"
	return f'''(?<=[{consonants}])([{vocals}]'''+"{2,})["+f'''{consonants}]'''

if __name__ == "__main__":
	matcher = re.findall(rf"{the_regex_expression()}", input(), flags = re.I)
	print('\n'.join(matcher or ['-1']))


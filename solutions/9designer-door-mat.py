#Url: https://www.hackerrank.com/challenges/designer-door-mat/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def designdoor(n,m):
	basemirror = [str(".|."*((i*2)+1)).center(m,'-') for i in range(n//2)]
	for i in range (len(basemirror)):
		print(basemirror[i])
	print ('WELCOME'.center(m,'-'))
	for i in range(len(basemirror)-1, -1, -1):
		print(basemirror[i])
	
if __name__ == '__main__':
	n, m = input().split()
	designdoor(int(n), int(m))
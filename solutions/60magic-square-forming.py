#Url: https://www.hackerrank.com/challenges/magic-square-forming/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

# Complete the formingMagicSquare function below.




def formingMagicSquare(s):
	the_mminimalcost, complete, i = 81, (s[0]+s[1]+s[2]), 0
	posible_orders = [
		[4,3,8,9,5,1,2,7,6],
		[4,9,2,3,5,7,8,1,6],
		[2,9,4,7,5,3,6,1,8],
		[2,7,6,9,5,1,4,3,8],
		[6,7,2,1,5,9,8,3,4],
		[6,1,8,7,5,3,2,9,4],
		[8,1,6,3,5,7,4,9,2],
		[8,3,4,1,5,9,6,7,2],
	]

	while i < len(posible_orders):
		j, tem = 0, 0
		while j < len(complete):
			if posible_orders[i][j] != complete[j]:
				tem += abs(posible_orders[i][j] - complete[j])
			j += 1
		if tem <= the_mminimalcost:
			the_mminimalcost = tem
		i += 1
	return the_mminimalcost

if __name__ == '__main__':
    s = []

    for _ in range(3):
        s.append(list(map(int, input().rstrip().split())))

    print(formingMagicSquare(s))
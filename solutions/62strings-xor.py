#Url: https://www.hackerrank.com/challenges/strings-xor/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

def strings_xor(s, t):
    res = ""
    for i in range(len(s)):
        if s[i] == t[i]:
            res += '0';
        else:
            res += '1';

    return res

s = input()
t = input()
print(strings_xor(s, t))

#Url: https://www.hackerrank.com/challenges/html-parser-part-1/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

from html.parser import HTMLParser

class TheHTMLParser(HTMLParser):
	#Is needed overwrite ....
    def handle_starttag(self, tag, attrs):
        print("Start : {0}".format(tag))
        for attribute in attrs:
            print ('->',attribute[0],'>',attribute[1])
    def handle_endtag(self, tag):
        print("End   : {0}".format(tag))
    def handle_startendtag(self, tag, attrs):
        print("Empty : {0}".format(tag))
        for attribute in attrs:
            print ('->',attribute[0],'>',attribute[1])

if __name__ == "__main__":
	parser = TheHTMLParser()
	the_lines = int(input())
	for i in range(the_lines):
	    parser.feed(input())
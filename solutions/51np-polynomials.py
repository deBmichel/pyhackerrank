#Url: https://www.hackerrank.com/challenges/np-polynomials/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
import numpy

if __name__ == "__main__":
	print (numpy.polyval(list(map(float, input().split())), int(input())))
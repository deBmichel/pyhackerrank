#Url: https://www.hackerrank.com/challenges/xml-1-find-the-score/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import sys
import xml.etree.ElementTree as etree

def get_attr_number(node):
	# The lecture in https://diveintopython3.net/xml.html was 
	# interesting for me... sure, section 12.4.2 speak about 
	# attrib method and tricks ....
	# The problem: recursive style, in the first test case 
	# failed with 5 expected 8, (Subnodes ?)
	# Recursion is pure beauty .....Sure...
	return len(node.attrib) + sum(get_attr_number(subnode) for subnode in node)
    

if __name__ == '__main__':
    sys.stdin.readline()
    xml = sys.stdin.read()
    tree = etree.ElementTree(etree.fromstring(xml))
    root = tree.getroot()
    print(get_attr_number(root))




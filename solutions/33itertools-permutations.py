#Url: https://www.hackerrank.com/challenges/itertools-permutations/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from itertools import permutations

if __name__ == "__main__":
	largs = input().split()
	word, k = largs[0], int(largs[1])
	combinations = list(permutations(word,k))
	combinations.sort()
	i, flagindex = 0, len(combinations)
	while i < flagindex:
		print("".join(combinations[i]))
		i += 1

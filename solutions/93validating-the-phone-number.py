#Url: https://www.hackerrank.com/challenges/validating-the-phone-number/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 
import re

def validate_number(number):
	if re.match(r'[789]\d{9}$',number):
		return True
	else:
		return False
			
if __name__ == "__main__":
	cases = int(input())
	for i in range(cases):
		if validate_number(input()):
			print ('YES')
		else:  
			print ('NO')




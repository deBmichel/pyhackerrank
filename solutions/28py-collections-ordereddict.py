#Url: https://www.hackerrank.com/challenges/py-collections-ordereddict/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from collections import OrderedDict

if __name__ == '__main__':
	items, i = int(input()), 0
	ordrd_dict = OrderedDict()
	while i < items:
		item = input().split(" ")
		item_name = " ".join(item[0:-1])
		if item_name in ordrd_dict:
			ordrd_dict[item_name] = ordrd_dict[item_name] + int(item[-1])
		else:
			ordrd_dict[item_name] = int(item[-1])
		i += 1
	for k, v in ordrd_dict.items():
		print (k, v)




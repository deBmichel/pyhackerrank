#Url: https://www.hackerrank.com/challenges/np-eye-and-identity/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	n, m = list(map(int, input().split()))
	
	numpy.set_printoptions(legacy='1.13')
	print(numpy.eye(n, m, k = 0) )

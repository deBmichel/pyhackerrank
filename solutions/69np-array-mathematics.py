#Url: https://www.hackerrank.com/challenges/np-array-mathematics/problem 
#Python Python 3.8.2 
#@Author: Michel M S

import numpy

if __name__ == "__main__":
	n, m = list(map(int, input().split()))
	a, b = [], list()
	for _ in range(n):
		a.append(list(map(int, input().split())))
	for _ in range(n):
		b.append(list(map(int, input().split())))
	anr = numpy.array(a, int)
	bnr = numpy.array(b, int)

	numpy.set_printoptions(legacy='1.13')

	print(numpy.add(anr, bnr))
	print(numpy.subtract(anr, bnr))
	print(numpy.multiply(anr, bnr))
	print(anr // bnr)
	print(numpy.mod(anr, bnr))
	print(numpy.power(anr, bnr))


	

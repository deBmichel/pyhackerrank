#Url: https://www.hackerrank.com/challenges/defaultdict-tutorial/problem?h_r=next-challenge 
#Python Python 3.8.2 
#@Author: Michel M S 

from collections import defaultdict

if __name__ == "__main__":
	n, m = list(map(int, input().split()))
	theDictA = defaultdict(list)
	i = 0
	while i < n:
		theDictA[input()].append(i + 1)
		i += 1

	i=0
	while i < m:
		blmnt = input()
		if blmnt in theDictA:
			print(*theDictA[blmnt])
		else:
			print("-1")
		i += 1

"""5 2
a
a
b
a
b
a
b
"""

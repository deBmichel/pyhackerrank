#Url: https://www.hackerrank.com/challenges/np-min-and-max/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

import numpy

if __name__ == "__main__":
	n, m = list(map(int, input().split()))
	my_array = numpy.array([list(map(int, input().split())) for _ in range(n) ])
	print(numpy.max(numpy.min(my_array, axis = 1)))




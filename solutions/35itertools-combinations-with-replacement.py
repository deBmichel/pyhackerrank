#Url: https://www.hackerrank.com/challenges/itertools-combinations-with-replacement/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

from itertools import combinations_with_replacement

def get_combination(word, k):
	newlist = [e for e in word]
	newlist.sort()

	Combs = list(combinations_with_replacement(newlist,k))
	Combs.sort()
	return Combs

if __name__ == "__main__":
	largs = input().split()
	word, K = largs[0], int(largs[1])
	combs = get_combination(word, K)
	i, flagindex = 0, len(combs)
	while i < flagindex:
		print("".join(combs[i]))
		i += 1
	

#Url: https://www.hackerrank.com/challenges/zipped/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	students , subjects = list(map(int, input().split()))
	thelac = list()
	for x in range(subjects):
		thelac.append(tuple(list(map(float, input().split()))))
	
	prepared = zip(*thelac)
	for e in prepared:
		print(round((sum(e)/subjects), 1))


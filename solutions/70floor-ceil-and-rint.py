#Url: https://www.hackerrank.com/challenges/floor-ceil-and-rint/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
import numpy 

if __name__ == "__main__":
	numpy.set_printoptions(legacy='1.13')
	arr = list(map(float, input().split()))
	thearr = numpy.array(arr)
	print(numpy.floor(thearr))
	print(numpy.ceil(thearr))
	print(numpy.rint(thearr))


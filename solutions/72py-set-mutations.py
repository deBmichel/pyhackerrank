#Url: https://www.hackerrank.com/challenges/py-set-mutations/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 


if __name__ == "__main__":
	input()
	orelements = set(input().split())
	instructions = int(input())
	for _ in range(instructions):
		instruction = input().split()
		if instruction[0] == "intersection_update":
			orelements.intersection_update(set(input().split()))
		elif instruction[0] == "update":
			orelements.update(set(input().split()))
		elif instruction[0] == "symmetric_difference_update":
			orelements.symmetric_difference_update(set(input().split()))
		elif instruction[0] == "difference_update":
			orelements.difference_update(set(input().split()))
	print(sum(list(map(int, orelements))))




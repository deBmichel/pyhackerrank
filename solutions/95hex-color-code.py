#Url: https://www.hackerrank.com/challenges/hex-color-code/problem? 
#Python Python 3.8.2 
#@Author: Michel M S
import re

matcher = lambda line : re.findall('[\s:](#[a-f0-9]{6}|#[a-f0-9]{3})', line, re.I)

if __name__ == "__main__":
	the_lines = int(input())
	colors =[]
	for _ in range(the_lines):
		colors += [e for e in matcher(input()) if e != "" and e != None]
	for e in colors:
		print(e)
	
	


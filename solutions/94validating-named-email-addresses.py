#Url: https://www.hackerrank.com/challenges/validating-named-email-addresses/problem? 
#Python Python 3.8.2 
#@Author: Michel M S

import re

matcher = lambda mail: re.match(r'<[A-Za-z](\w|-|\.|_)+@[A-Za-z]+\.[A-Za-z]{1,3}>', mail)

if __name__ == "__main__":
	n = int(input())
	for _ in range(n):
		alias, mail = input().split(' ')
		if matcher(mail):
			print(alias,mail)


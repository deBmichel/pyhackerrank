#Url: https://www.hackerrank.com/challenges/find-angle/problem?h_r=next-challenge 
#Python Python 3.8.2 
#@Author: Michel M S 
from math import atan, degrees

if __name__=="__main__":
	print(f'{round(degrees(atan(int(input())/int(input()))))}°')

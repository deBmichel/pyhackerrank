#Url: https://www.hackerrank.com/challenges/py-set-symmetric-difference-operation/problem? 
#Python Python 3.8.2 
#@Author: Michel M S

if __name__ == "__main__":
	input()
	a = set(list(map(int, input().split())))
	input()
	b = set(list(map(int, input().split())))
	print (len(a.symmetric_difference(b)))


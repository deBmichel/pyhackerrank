#Url: https://www.hackerrank.com/challenges/piling-up/problem 
#Python Python 3.8.2 
#@Author: Michel M S
from collections import deque

if __name__ == "__main__":
	cases = int(input())
	for t in range(cases):
		_, pildq, posible =input(), deque(list(map(int, input().split(" ")))), True
		i, j = pildq[0], pildq[len(pildq) - 1]
		while True:
			if len(pildq) >= 2:
				if pildq[0] <= i:
					i, j = pildq[0], pildq[len(pildq) - 1]
					if len(pildq) >= 2:
						pildq.pop()
						pildq.popleft()
					elif len(pildq) == 1:
						pildq.pop()
					else:
						break
					if len(pildq) == 0:
						break
				else:
					posible = False
					break
			else:
				if pildq[0] <= i or pildq[-1] <= j:
					break
				else:
					posible = False
					break
		if posible:
			print("Yes")
		else:
			print("No")
		

"""
A faster but not of my brain solution:
for t in range(int(input())):
    input()
    lst = list(map(int, input().split()))
    l = len(lst)
    i = 0
    while i < (l - 1) and lst[i] >= lst[i+1]:
        i += 1

    print(i)

    while i < (l - 1) and lst[i] <= lst[i+1]:
        i += 1
	
    print(i)

    if i == l - 1:
    	print ("Yes")
    else:
    	print ("No")
"""




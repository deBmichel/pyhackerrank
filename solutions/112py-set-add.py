#Url: https://www.hackerrank.com/challenges/py-set-add/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	ammount = int(input())
	stamps = set()
	for _ in range(ammount):
		stamps.add(input())
	print(len(stamps))
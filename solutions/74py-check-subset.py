#Url: https://www.hackerrank.com/challenges/py-check-subset/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

if __name__ == "__main__":
	cases = int(input())
	for _ in range(cases):
		sizeb, a_set = input(), set(list(map(int, input().split())))
		sizeb, b_set = input(), set(list(map(int, input().split())))
		if a_set.intersection(b_set) == a_set:
			print(True)
		else:
			print(False)

#Url: https://www.hackerrank.com/challenges/any-or-all/problem? 
#Python Python 3.8.2 
#@Author: Michel M S 

# 3 lines challenge:
size, numbers = input(), input().split()
print(all([int(e) >= 0 for e in numbers]) and any([e[0]==e[-1] for e in numbers]))
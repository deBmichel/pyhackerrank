#Url: https://www.hackerrank.com/challenges/cats-and-a-mouse/problem 
#Python Python 3.8.2 
#@Author: Michel M S 

#!/bin/python3

# Complete the catAndMouse function below.
def catAndMouse(x, y, z):
	if abs(x - z) == abs(y - z):
		return "Mouse C"
	if abs(x - z) < abs(y - z):
		return "Cat A"
	if abs(y - z) < abs(x - z):
		return "Cat B"


if __name__ == '__main__':
    q = int(input())

    for q_itr in range(q):
        xyz = input().split()

        x = int(xyz[0])

        y = int(xyz[1])

        z = int(xyz[2])

        print(catAndMouse(x, y, z))

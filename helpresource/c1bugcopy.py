#Url: https://www.hackerrank.com/contests/bug/challenges/fateful-minions/problem 
#Python Python 3.8.2 
#@Author: Michel M S 
from collections import deque
import re

def get_north(x, y, d):
	if y-d >= 0:
		return ((x), (y-d))
	
def get_east(x, y, d, M):
	if x + d < M:
		return ((x+d), (y))
	
def get_south(x, y, d, N):
	if y+d < N:
		return ((x), (y+d))
	
def get_west(x, y, d):
	if x-d >= 0:
		return ((x-d), (y))
	
def get_northeast(x, y, d, M):
	if (y-d >= 0) and (x+d < M):
		return ((x+d), (y-d))

def get_northwest(x, y, d):
	if (y-d >= 0) and (x-d >= 0):
		return ((x-d), (y-d))

def get_southeast(x, y, d, N, M):
	if (y+d < N) and (x-d < M):
		return ((x+d), (y+d))

def get_southwest(x, y, d, N):
	if (y+d < N) and (x-d >= 0):
		return ((x-d) , (y+d))

def get_allpoints_fori(x, y, d, N, M, alls):
	north = get_north(x, y, d)
	if north != None and (not north in alls):
		alls.append(north)

	northeast = get_northeast(x, y, d, M)
	if northeast != None and (not northeast in alls):
		alls.append(northeast)

	east = get_east(x, y, d, M)
	if east != None and (not east in alls):
		alls.append(east)

	southeast = get_southeast(x, y, d, N, M)
	if southeast != None and (not southeast in alls):
		alls.append(southeast)

	south = get_south(x, y, d, N)
	if south != None and (not south in alls):
		alls.append(south)

	southwest = get_southwest(x, y, d, N)
	if southwest != None and (not southwest in alls):
		alls.append(southwest)

	west = get_west(x, y, d)
	if west != None and (not west in alls):
		alls.append(west)
	
	northwest = get_northwest(x, y, d)
	if northwest != None and (not northwest in alls):
		alls.append(northwest)

	return alls
	
def run():
	cases, i = int(input()), 0
	stinkpoint = deque()	
	while i < cases:
		stinkpoints = deque()	
		N,M,d,X = map(int,input().split())
		iN = 0
		while iN < N:
			line = input()
			a_matches = re.finditer("\*", line)
			a_indexes = [match.start() for match in a_matches]
			if a_indexes:
				i_aindxs, i_aindxsfl = 0, len(a_indexes)
				while i_aindxs < i_aindxsfl:
					id = 0
					while id <= d:
						stinkpoints = get_allpoints_fori(a_indexes[i_aindxs], iN, id, N, M, stinkpoints)
						id += 1
					i_aindxs += 1
			iN += 1
		i += 1
		nostinkpoints = (N * M) - len(stinkpoints)
		if nostinkpoints >= X:
			print(0)
		else:
			print(X - nostinkpoints)
		
"""
1
5 5 2 15
.*...
.....
.....
...*.
.....

1
5 5 2 15
***..
**...
*.*.*
...**
..***

15 - 12

 (1, 3)


 0 1 2 3 4 5 6 7 8 9
0 | | | | | | | | | |
1 |1| |x| |x| | | | |
2 | | | | | | | | | |
3 |3| |x| |x| | | | |
4 | | | | | | | | | |
5 |v| |x| |x| | | | |
6 | | | | | | | | | |
7 | | | | | | | | | |
8 | | | | | | | | | |
9 | | | | | | | | | |





		"""
		

	



